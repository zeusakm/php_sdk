# SDK for nocodeapi.net ML services

### Installation
Preferred way to install is with Composer.

perform command in shell

```bash
 composer require nocodeapidotnet/php_sdk
```

OR

just add

```javascript
"require": {
  "nocodeapidotnet/php_sdk": ">=0.1"
}
```

to your projects composer.json.

### Configure
```php
use NoCodeApi\Client;
use NoCodeApi\ClientConfig;

$config = new ClientConfig($_ENV['NOCODEAPI_JWT_TOKEN']);

$this->client = new Client($config);
```

### Key Phrase Extraction API
```php
/** @var \NoCodeApi\Component\KeyPhrases $keyPhrases */
$keyPhrases = $this->client->getKeyPhrases("Now, as a nation, we don't promise equal outcomes, but we were founded on the idea everybody should have an equal opportunity to succeed. No matter who you are, what you look like, where you come from, you can make it. That's an essential promise.");

$keyPhrases->getKeyPhrases();

// Data returned by getKeyPhrases() method example
/**
    [
        {
            "Text": "promise equal outcomes",
            "Score": 8,
            "BeginOffset": 27,
            "EndOffset": 49
        },
        {
            "Text": "equal opportunity",
            "Score": 4.5,
            "BeginOffset": 108,
            "EndOffset": 125
        },
        {
            "Text": "essential promise",
            "Score": 4.5,
            "BeginOffset": 229,
            "EndOffset": 246
        },
        {
            "Text": "nation",
            "Score": 1,
            "BeginOffset": 10,
            "EndOffset": 16
        },
        {
            "Text": "founded",
            "Score": 1,
            "BeginOffset": 63,
            "EndOffset": 70
        },
        {
            "Text": "idea",
            "Score": 1,
            "BeginOffset": 78,
            "EndOffset": 82
        },
        {
            "Text": "succeed",
            "Score": 1,
            "BeginOffset": 129,
            "EndOffset": 136
        },
        {
            "Text": "matter",
            "Score": 1,
            "BeginOffset": 141,
            "EndOffset": 147
        },
        {
            "Text": "make",
            "Score": 1,
            "BeginOffset": 210,
            "EndOffset": 214
        },
        {
            "Text": "america",
            "Score": 1,
            "BeginOffset": false,
            "EndOffset": 7
        },
        {
            "Text": "start",
            "Score": 1,
            "BeginOffset": 269,
            "EndOffset": 274
        },
        {
            "Text": "determine",
            "Score": 1,
            "BeginOffset": 286,
            "EndOffset": 295
        },
        {
            "Text": "end",
            "Score": 1,
            "BeginOffset": 306,
            "EndOffset": 309
        },
        {
            "Text": "read",
            "Score": 1,
            "BeginOffset": false,
            "EndOffset": 4
        }
    ]
**/
```

### Language Detection API
```php
/** @var NoCodeApi\Entity\Language $lang */
$lang = $this->client->getLanguageDetection('Policjanci otrzymali zgłoszenie w tej sprawie po godz 9. Do wypadku doszło na ul. Kolonia Krakowskie Przedmieście.');

$lang->getLanguageCode(); // pl
$lang->getLanguage(); // Poland
$lang->getScore(); // 1
```

### Sentiment Analysis API
```php
/** @var NoCodeApi\Component\Emotions $emotions */
$emotions = $this->client->getEmotions('Genuine leather is the best production for bags imho, but I would not recommend this particular product.');

$emotions->getPositive(); // 0.12
$emotions->getNegative(); // 0.14
$emotions->getNeutral(); // 9.74
$emotions->getMixed(); // 9.97
```

### Custom classification API
```php
// Create by adding classification text and it's label
$this->client->createClassification('foo_bar_baz', 'It was actually different, but the most profitable part was at start'); // true/false

// Update 
$this->client->updateClassification('foo_bar_baz', 'It was actually different, but the most profitable part was at start and in the end of a journey'); // true/false

// Classify the text
/** @var NoCodeApi\Entity\Classification $classification */ 
$classification = $this->client->getClassification('profitable part');

$classification->getLabel(); // getting the label of that successfully classified text

// Deleting classification
$this->client->deleteClassification('foo_bar_baz'); // true/false
```

### Named Entity Recognition
```php
$namedEntities = $this->client->getNamedEntities('Linus Torvalds is one of the best programmers in USA, Finland Helsinki and in the world');

$namedEntities->getEntitiesCount(); // 3

$namedEntities->getNamedEntities(); 
/**
[
    [
        "Text" => "Linus Torvalds",
        "Label" => "PERSON"
    ],
    [
        "Text" => "USA",
        "Label" => "GPE"
    ],
    [
        "Text" => "Finland Helsinki",
        "Label" => "GPE"
    ],
]
**/
```