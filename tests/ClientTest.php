<?php


namespace NoCodeApi\Tests;


use Dotenv\Dotenv;
use NoCodeApi\Client;
use NoCodeApi\ClientConfig;
use NoCodeApi\Contract\ClientContract;
use NoCodeApi\Entity\NamedEntity;
use PHPUnit\Framework\TestCase;

class ClientTest extends TestCase
{

    private Client $client;

    public function setUp(): void
    {
        $dotenv = Dotenv::createImmutable(__DIR__);
        $dotenv->load();
        $config = new ClientConfig($_ENV['NOCODEAPI_JWT_TOKEN']);

        $this->client = new Client($config);
    }

    /**
     * @test
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function it_gets_key_phrases(): void
    {
        $keyPhrases = $this->client->getKeyPhrases("Now, as a nation, we don't promise equal outcomes, but we were founded on the idea everybody should have an equal opportunity to succeed. No matter who you are, what you look like, where you come from, you can make it. That's an essential promise.");

        self::assertEquals(ClientContract::DEFAULT_LANGUAGE, $keyPhrases->getLanguageCode());
        self::assertNotEmpty($keyPhrases->getKeyPhrases());
    }

    /**
     * @test
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function it_gets_emotions(): void
    {
        $emotions = $this->client->getEmotions('Genuine leather is the best production for bags imho, but I would not recommend this particular product.');

        self::assertGreaterThan(0, $emotions->getPositive());
        self::assertGreaterThan(0, $emotions->getNegative());
        self::assertGreaterThan(0, $emotions->getMixed());
        self::assertGreaterThan(0, $emotions->getNeutral());
    }

    /**
     * @test
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function it_detects_language(): void
    {
        $lang = $this->client->getLanguageDetection('Policjanci otrzymali zgłoszenie w tej sprawie po godz 9. Do wypadku doszło na ul. Kolonia Krakowskie Przedmieście.');

        self::assertEquals('pl', $lang->getLanguageCode());
        self::assertEquals('Polish', $lang->getLanguage());
        self::assertEquals(1, $lang->getScore());
    }

    /**
     * @test
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function it_cruds_classification(): void
    {
        $classificationCreate = $this->client->createClassification('foo_bar_baz', 'It was actually different, but the most profitable part was at start');
        self::assertTrue($classificationCreate);
        $classificationUpdate = $this->client->updateClassification('foo_bar_baz', 'It was actually different, but the most profitable part was at start and in the end of a journey');
        self::assertTrue($classificationUpdate);
        $classification = $this->client->getClassification('profitable part');
        self::assertEquals('foo_bar_baz', $classification->getLabel());
        $classificationDelete = $this->client->deleteClassification('foo_bar_baz');
        self::assertTrue($classificationDelete);
    }

    /**
     * @test
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function it_gets_named_entities(): void
    {
        $namedEntities = $this->client->getNamedEntities('Linus Torvalds is one of the best programmers in USA, Finland Helsinki and in the world');
        self::assertEquals(3, $namedEntities->getEntitiesCount());
        self::assertEquals([
            (new NamedEntity())->setText('Linus Torvalds')->setLabel('PERSON'),
            (new NamedEntity())->setText('USA')->setLabel('GPE'),
            (new NamedEntity())->setText('Finland Helsinki')->setLabel('GPE'),
        ], $namedEntities->getNamedEntities());
    }
}