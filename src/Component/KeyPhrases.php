<?php

namespace NoCodeApi\Component;

use NoCodeApi\Entity\KeyPhrase;

/**
 * Class KeyPhrases
 * @package NoCodeApi\Components
 */
class KeyPhrases
{
    /**
     * @var string
     */
    private $languageCode;

    /**
     * @var KeyPhrase[]
     */
    private $keyPhrases;

    /**
     * KeyPhrases constructor.
     * @param KeyPhrase[] $keyPhrases
     * @param string $languageCode
     */
    public function __construct(array $keyPhrases, string $languageCode)
    {
        $this->keyPhrases = $keyPhrases;
        $this->languageCode = $languageCode;
    }

    /**
     * @return string
     */
    public function getLanguageCode(): string
    {
        return $this->languageCode;
    }

    /**
     * @return KeyPhrase[]
     */
    public function getKeyPhrases(): array
    {
        return $this->keyPhrases;
    }
}