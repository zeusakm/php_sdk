<?php


namespace NoCodeApi\Component;

use NoCodeApi\Entity\NamedEntity;

class NamedEntities
{
    /**
     * @var int
     */
    private $entitiesCount;

    /**
     * @var NamedEntity[]
     */
    private $namedEntities;

    /**
     * @param int $entitiesCount
     * @return NamedEntities
     */
    public function setEntitiesCount(int $entitiesCount): NamedEntities
    {
        $this->entitiesCount = $entitiesCount;
        return $this;
    }

    /**
     * @return int
     */
    public function getEntitiesCount(): int
    {
        return $this->entitiesCount;
    }

    /**
     * @param NamedEntity[] $namedEntities
     * @return NamedEntities
     */
    public function setNamedEntities(array $namedEntities): NamedEntities
    {
        $this->namedEntities = [];
        if (!empty($namedEntities)) {
            foreach ($namedEntities as $entity) {
                $this->namedEntities[] = (new NamedEntity())->setText($entity['Text'])->setLabel($entity['Label']);
            }
        }

        return $this;
    }

    /**
     * @return NamedEntity[]
     */
    public function getNamedEntities(): array
    {
        return $this->namedEntities;
    }
}