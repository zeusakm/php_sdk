<?php

namespace NoCodeApi\Component;

/**
 * Class Emotions
 * @package NoCodeApi\Component
 */
class Emotions
{
    /**
     * @var double
     */
    private $positive;

    /**
     * @var double
     */
    private $negative;

    /**
     * @var double
     */
    private $neutral;

    /**
     * @var double
     */
    private $mixed;

    /**
     * @param mixed $positive
     * @return Emotions
     */
    public function setPositive($positive): Emotions
    {
        $this->positive = $positive;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getPositive()
    {
        return $this->positive;
    }

    /**
     * @param float $negative
     * @return Emotions
     */
    public function setNegative(float $negative): Emotions
    {
        $this->negative = $negative;
        return $this;
    }

    /**
     * @return float
     */
    public function getNegative(): float
    {
        return $this->negative;
    }

    /**
     * @param float $neutral
     * @return Emotions
     */
    public function setNeutral(float $neutral): Emotions
    {
        $this->neutral = $neutral;
        return $this;
    }

    /**
     * @return float
     */
    public function getNeutral(): float
    {
        return $this->neutral;
    }

    /**
     * @param float $mixed
     * @return Emotions
     */
    public function setMixed(float $mixed): Emotions
    {
        $this->mixed = $mixed;
        return $this;
    }

    /**
     * @return float
     */
    public function getMixed(): float
    {
        return $this->mixed;
    }
}