<?php

namespace NoCodeApi;

/**
 * Class ClientConfig
 * @package NoCodeApi
 */
class ClientConfig
{
    private $baseUri = 'https://nocodeapi.net/api/';

    private $timeOut = 25.0;

    /**
     * @var string
     */
    private $jwtToken;

    public const URI_KEY_PHRASES = 'key-phrases';
    public const URI_LANGUAGE_DETECTION = 'language-detection';
    public const URI_EMOTIONS_DETECTION = 'emotions';
    public const URI_CLASSIFICATION = 'classification';
    public const URI_CLASSIFY = 'classify';
    public const URI_NAMED_ENTITY = "named-entity-recognition";

    public function __construct(string $jwt)
    {
        $this->jwtToken = $jwt;
    }

    /**
     * @return string
     */
    public function getBaseUri(): string
    {
        return $this->baseUri;
    }

    /**
     * @return float
     */
    public function getTimeOut(): float
    {
        return $this->timeOut;
    }

    /**
     * @return string
     */
    public function getJwtToken(): string
    {
        return $this->jwtToken;
    }
}
