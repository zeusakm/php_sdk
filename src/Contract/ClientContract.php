<?php

namespace NoCodeApi\Contract;

use NoCodeApi\Component\Emotions;
use NoCodeApi\Component\KeyPhrases;
use NoCodeApi\Entity\Classification;
use NoCodeApi\Entity\Language;

/**
 * Interface ClientContract
 * @package NoCodeApi\Contract
 */
interface ClientContract
{
    public const DEFAULT_LANGUAGE = 'en_US';

    /**
     * @param string $text
     * @param string $lang
     * @param int $phraseMinLength
     * @return mixed
     */
    public function getKeyPhrases(string $text, string $lang = self::DEFAULT_LANGUAGE, $phraseMinLength = 0): KeyPhrases;

    /**
     * @param string $text
     * @return Emotions
     */
    public function getEmotions(string $text): Emotions;

    /**
     * @param string $text
     * @param string $lang
     * @return Classification
     */
    public function getClassification(string $text, string $lang): Classification;

    /**
     * @param string $text
     * @return Language
     */
    public function getLanguageDetection(string $text): Language;

    /**
     * @param string $text
     * @param string $label
     * @return bool
     */
    public function createClassification(string $text, string $label): bool;

    /**
     * @param string $text
     * @param string $label
     * @param string $lang
     * @return bool
     */
    public function updateClassification(string $text, string $label, $lang = self::DEFAULT_LANGUAGE): bool;

    /**
     * @param string $label
     * @return bool
     */
    public function deleteClassification(string $label): bool;
}