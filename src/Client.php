<?php


namespace NoCodeApi;

use GuzzleHttp\Exception\GuzzleException;
use NoCodeApi\Component\Emotions;
use NoCodeApi\Component\KeyPhrases;
use NoCodeApi\Component\NamedEntities;
use NoCodeApi\Contract\ClientContract;
use NoCodeApi\Entity\Classification;
use NoCodeApi\Entity\KeyPhrase;
use NoCodeApi\Entity\Language;
use Psr\Http\Message\ResponseInterface;

/**
 * Class Client
 * @package NoCodeApi
 */
class Client implements ClientContract
{
    /**
     * @var ClientConfig|null
     */
    private ?ClientConfig $config;
    /**
     * @var \GuzzleHttp\Client
     */
    private \GuzzleHttp\Client $client;

    /**
     * Client constructor.
     * @param null|ClientConfig $config
     */
    public function __construct(ClientConfig $config = null)
    {
        $this->config = $config;

        $this->client = new \GuzzleHttp\Client([
            // Base URI is used with relative requests
            'base_uri' => $this->config->getBaseUri(),
            // You can set any number of default request options.
            'timeout' => $this->config->getTimeOut(),
            'debug' => true,
        ]);
    }

    /**
     * @param string $text
     * @param string $lang
     * @param int $phraseMinLength
     * @return KeyPhrases
     * @throws GuzzleException
     */
    public function getKeyPhrases(string $text, string $lang = self::DEFAULT_LANGUAGE, $phraseMinLength = 0): KeyPhrases
    {
        $resp = $this->client->get(ClientConfig::URI_KEY_PHRASES . '?' . http_build_query(
                [
                    'text' => $text,
                    'lang' => $lang,
                    'min' => $phraseMinLength,
                    'jwt' => $this->config->getJwtToken(),
                ]));

        $keyPhrasesContent = $this->toArray($resp);

        $keyPhrases = [];
        foreach ($keyPhrasesContent['KeyPhrases'] as $value) {
            $keyPhrases[] = (new KeyPhrase())->setText($value['Text'])
                ->setScore($value['Score'])
                ->setBeginOffset($value['BeginOffset'])
                ->setEndOffset($value['EndOffset']);
        }

        return new KeyPhrases($keyPhrases, $keyPhrasesContent['LanguageCode']);
    }

    /**
     * @param string $text
     * @return Emotions
     * @throws GuzzleException
     */
    public function getEmotions(string $text): Emotions
    {
        $resp = $this->client->get(ClientConfig::URI_EMOTIONS_DETECTION . '?' . http_build_query(
                [
                    'text' => $text,
                    'lang' => 'en',
                    'jwt' => $this->config->getJwtToken(),
                ]));

        $content = $this->toArray($resp);

        return (new Emotions())->setPositive($content['Positive'])->setNegative($content['Negative'])
            ->setNeutral($content['Neutral'])->setMixed($content['Mixed']);
    }

    /**
     * @param string $text
     * @return Language
     * @throws GuzzleException
     */
    public function getLanguageDetection(string $text): Language
    {
        $resp = $this->client->get(ClientConfig::URI_LANGUAGE_DETECTION . '?' . http_build_query(
                [
                    'text' => $text,
                    'jwt' => $this->config->getJwtToken(),
                ]));

        $content = $this->toArray($resp);

        return (new Language())->setLanguageCode($content['LanguageCode'])
            ->setLanguage($content['Language'])->setScore($content['Score']);
    }

    /**
     * @param string $text
     * @param string $lang
     * @return Classification
     * @throws GuzzleException
     */
    public function getClassification(string $text, string $lang = self::DEFAULT_LANGUAGE): Classification
    {
        $resp = $this->client->get(ClientConfig::URI_CLASSIFICATION . '?' . http_build_query(
                [
                    'text' => $text,
                    'lang' => $lang,
                    'jwt' => $this->config->getJwtToken(),
                ]));

        $content = $this->toArray($resp);

        return (new Classification())->setText($content['Text'])->setLabel($content['Label']);
    }

    /**
     * @param string $text
     * @param string $label
     * @param string $lang
     * @return bool
     * @throws GuzzleException
     */
    public function createClassification(string $label, string $text, $lang = self::DEFAULT_LANGUAGE): bool
    {
        $resp = $this->client->post(ClientConfig::URI_CLASSIFY, [
            'form_params' => [
                'label' => $label,
                'text' => $text,
                'lang' => $lang,
                'jwt' => $this->config->getJwtToken(),
            ]
        ]);

        $content = $this->toArray($resp);

        return $content['Success'];
    }

    /**
     * @param string $text
     * @param string $label
     * @param string $lang
     * @return bool
     * @throws GuzzleException
     */
    public function updateClassification(string $label, string $text, $lang = self::DEFAULT_LANGUAGE): bool
    {
        $resp = $this->client->patch(ClientConfig::URI_CLASSIFY, [
            'form_params' => [
                'label' => $label,
                'text' => $text,
                'lang' => $lang,
                'jwt' => $this->config->getJwtToken(),
            ]
        ]);

        $content = $this->toArray($resp);

        return $content['Success'];
    }

    /**
     * @param string $label
     * @param string $lang
     * @return bool
     * @throws GuzzleException
     */
    public function deleteClassification(string $label, string $lang = self::DEFAULT_LANGUAGE): bool
    {
        $resp = $this->client->delete(ClientConfig::URI_CLASSIFY, [
            'form_params' => [
                'label' => $label,
                'jwt' => $this->config->getJwtToken(),
            ]
        ]);

        $content = $this->toArray($resp);

        return $content['Success'];
    }

    /**
     * @param string $text
     * @return NamedEntities
     * @throws GuzzleException
     */
    public function getNamedEntities(string $text): NamedEntities
    {
        $resp = $this->client->get(ClientConfig::URI_NAMED_ENTITY . '?' . http_build_query(
                [
                    'text' => $text,
                    'jwt' => $this->config->getJwtToken(),
                ]));

        $content = $this->toArray($resp);

        return (new NamedEntities())->setEntitiesCount($content['EntitiesCount'])
            ->setNamedEntities($content['Entities']);
    }

    /**
     * @param ResponseInterface $response
     * @return array
     * @throws \JsonException
     */
    private function toArray(ResponseInterface $response): array
    {
        return json_decode($response->getBody()->getContents(), true, 512, JSON_THROW_ON_ERROR);
    }
}