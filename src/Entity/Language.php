<?php

namespace NoCodeApi\Entity;

/**
 * Class Language
 * @package NoCodeApi\Entity
 */
class Language
{
    /**
     * @var string
     */
    private $languageCode;

    /**
     * @var string
     */
    private $language;

    /**
     * @var float
     */
    private $score;

    /**
     * @param string $languageCode
     * @return Language
     */
    public function setLanguageCode(string $languageCode): Language
    {
        $this->languageCode = $languageCode;
        return $this;
    }

    /**
     * @return string
     */
    public function getLanguageCode(): string
    {
        return $this->languageCode;
    }

    /**
     * @param string $language
     * @return Language
     */
    public function setLanguage(string $language): Language
    {
        $this->language = $language;
        return $this;
    }

    /**
     * @return string
     */
    public function getLanguage(): string
    {
        return $this->language;
    }

    /**
     * @param float $score
     * @return Language
     */
    public function setScore(float $score): Language
    {
        $this->score = $score;
        return $this;
    }

    /**
     * @return float
     */
    public function getScore(): float
    {
        return $this->score;
    }
}
