<?php


namespace NoCodeApi\Entity;

/**
 * Class Classification
 * @package NoCodeApi\Entity
 */
class Classification
{
    /**
     * @var string
     */
    private $text;

    /**
     * @var string
     */
    private $label;

    /**
     * @param string $text
     * @return Classification
     */
    public function setText(string $text): Classification
    {
        $this->text = $text;
        return $this;
    }

    /**
     * @param string $label
     * @return Classification
     */
    public function setLabel(string $label): Classification
    {
        $this->label = $label;
        return $this;
    }

    /**
     * @return string
     */
    public function getText(): string
    {
        return $this->text;
    }

    /**
     * @return string
     */
    public function getLabel(): string
    {
        return $this->label;
    }
}
