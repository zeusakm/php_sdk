<?php

namespace NoCodeApi\Entity;

/**
 * Class KeyPhrase
 * @package NoCodeApi\Entity
 */
class KeyPhrase
{
    /**
     * @var string
     */
    private $text;

    /**
     * @var double
     */
    private $score;

    /**
     * @var int
     */
    private $beginOffset;

    /**
     * @var int
     */
    private $endOffset;

    /**
     * @return string
     */
    public function getText(): string
    {
        return $this->text;
    }

    /**
     * @param string $text
     * @return KeyPhrase
     */
    public function setText(string $text): KeyPhrase
    {
        $this->text = $text;
        return $this;
    }

    /**
     * @return float
     */
    public function getScore(): float
    {
        return $this->score;
    }

    /**
     * @param float $score
     */
    public function setScore(float $score): KeyPhrase
    {
        $this->score = $score;
        return $this;
    }

    /**
     * @param int $beginOffset
     * @return KeyPhrase
     */
    public function setBeginOffset(int $beginOffset): KeyPhrase
    {
        $this->beginOffset = $beginOffset;
        return $this;
    }

    /**
     * @return int
     */
    public function getBeginOffset(): int
    {
        return $this->beginOffset;
    }

    /**
     * @param int $endOffset
     * @return KeyPhrase
     */
    public function setEndOffset(int $endOffset): KeyPhrase
    {
        $this->endOffset = $endOffset;
        return $this;
    }

    /**
     * @return int
     */
    public function getEndOffset(): int
    {
        return $this->endOffset;
    }
}