<?php


namespace NoCodeApi\Entity;


class NamedEntity
{
    /**
     * @var string
     */
    private $text;

    /**
     * @var string
     */
    private $label;

    /**
     * @param string $text
     * @return NamedEntity
     */
    public function setText(string $text): NamedEntity
    {
        $this->text = $text;
        return $this;
    }

    /**
     * @return string
     */
    public function getText(): string
    {
        return $this->text;
    }

    /**
     * @param string $label
     * @return NamedEntity
     */
    public function setLabel(string $label): NamedEntity
    {
        $this->label = $label;
        return $this;
    }

    /**
     * @return string
     */
    public function getLabel(): string
    {
        return $this->label;
    }
}